import time
import os
import math
import csv
import traceback
import logging

import colorlog

LOGGER_LEVEL = logging.DEBUG
logger = logging.getLogger("fileLogger")


def init_logger(folder):
    global logger
    # Clearing any previous handles if they exist (useful when calling this function more than once per run)
    handlers = logger.handlers[:]
    for handler in handlers:
        handler.close()
        logger.removeHandler(handler)
    # Create the folder if it doesnt exist
    os.makedirs(os.path.dirname(folder), exist_ok=True)
    filename = folder + "main.log"
    log_format_full = "%(asctime)s - %(levelname)s - %(message)s"
    log_format = "%(message)s"
    bold_seq = "\033[1m"
    colorlog_format = f"{bold_seq} " "%(log_color)s " f"{log_format}"
    colorlog.basicConfig(format=colorlog_format)
    # create logger
    logger = logging.getLogger("fileLogger")
    logger.setLevel(LOGGER_LEVEL)
    # # create console handler and set level to debug (raplaced by the above colorlog)
    # ch = logging.StreamHandler()
    # ch.setLevel(LOGGER_LEVEL)
    # create file handler and set level to debug
    fh = logging.FileHandler(filename)
    fh.setLevel(LOGGER_LEVEL)
    # create formatter
    formatter = logging.Formatter(log_format_full)

    # add formatter to ch and fh
    # ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add ch and fh to logger
    # logger.addHandler(ch)
    logger.addHandler(fh)
    logger.info("Logger initialized. Log file will be written at {}".format(filename))
    logger.debug("Debugs will be dispayed like this")
    logger.warn("Warnings will be dispayed like this")
    logger.error(
        "Errors will be displayed like this (some red when starting the code so you're reminded of Gazebo"
    )


def round_decimals_up(number, decimals=8):
    # 8 decimals is a satoshi
    if decimals == 0:
        return math.ceil(number)

    factor = 10 ** decimals
    return math.ceil(number * factor) / factor


def round_decimals_down(number, decimals=8):
    # 8 decimals is a satoshi
    if decimals == 0:
        return math.floor(number)

    factor = 10 ** decimals
    return math.floor(number * factor) / factor


def float_to_string(number, precision=8):
    return (
        "{0:.{prec}f}".format(
            number,
            prec=precision,
        )
        .rstrip("0")
        .rstrip(".")
        or "0"
    )


def float_to_string_up(number, precision=8):
    number = round_decimals_up(number, decimals=8)
    return (
        "{0:.{prec}f}".format(
            number,
            prec=precision,
        )
        .rstrip("0")
        .rstrip(".")
        or "0"
    )


def float_to_string_down(number, precision=8):
    number = round_decimals_down(number, decimals=8)
    return (
        "{0:.{prec}f}".format(
            number,
            prec=precision,
        )
        .rstrip("0")
        .rstrip(".")
        or "0"
    )


if __name__ == "__main__":
    s = 0.00000001
    a = 1.000000231
    b = 1.000000239
    print("a={}, float_to_string(a)={}".format(a, float_to_string(a)))
    print("a={}, float_to_string_up(a)={}".format(a, float_to_string_up(a)))
    print("a={}, float_to_string_down(a)={}".format(a, float_to_string_down(a)))
    print("b={}, float_to_string(b)={}".format(b, float_to_string(b)))
    print("b={}, float_to_string_up(b)={}".format(b, float_to_string_up(b)))
    print("b={}, float_to_string_down(b)={}".format(b, float_to_string_down(b)))
