from binance.client import Client
from binance.enums import *
from binance.exceptions import *
import sys
import math
import json
import requests
import webbrowser
import time
import urllib
import os
import ssl
import traceback
import logging
import datetime

from utils import *

logger = logging.getLogger("fileLogger")
# 1 satoshi = 0.00000001 bitcoin
# https://www.binance.com/en/trade-rule
common_min_order_size = 0.0001


def pump_and_dump():
    full_date = datetime.datetime.now().strftime("%Y-%m-%d_%H_%M_%S")
    folder = "./logs/" + full_date + "/"
    os.makedirs(os.path.dirname(folder), exist_ok=True)
    init_logger(folder)

    config_file = "config.json"
    logger.info("Loading {}...".format(config_file))
    f = open(config_file)
    data = json.load(f)
    # loading config settings
    quotedCoin = data["quotedCoin"]
    buyLimit = data["buyLimit"]
    percentOfWallet = float(data["percentOfWallet"]) / 100
    manualQuoted = float(data["manualQuoted"])
    profitMargin = float(data["profitMargin"]) / 100
    stopLoss = float(data["stopLoss"])
    limitLoss = float(data["limitLoss"])
    currentVersion = float(data["currentVersion"])
    endpoint = data["endpoint"]
    is_simulation = data["isSimulation"]
    if stopLoss <= 0:
        logger.error("stopLoss needs to be >0 in config.json")
        sys.exit()
    if stopLoss < limitLoss:
        logger.error("stopLoss needs to be >= limitLoss in config.json")
        sys.exit()
    if type(is_simulation) != bool:
        message = (
            "'isSimulation' field in config.json not understood. Use true or false"
        )
        logger.info(message)
        sys.exit(message)

    if is_simulation:
        endpoints = {
            "default": "https://testnet.binance.vision/api",
        }
    else:
        endpoints = {
            "default": "https://api.binance.{}/api",
            "api1": "https://api1.binance.{}/api",
            "api2": "https://api2.binance.{}/api",
            "api3": "https://api3.binance.{}/api",
        }

    # read json file
    try:
        if is_simulation:
            key_path = "keys_simu.json"
            f = open(key_path)
        else:
            key_path = "keys_real.json"
            f = open(key_path)
    except Exception as e:
        logger.error("Exception : '{}'".format(e))
        logger.error("Traceback : '{}'".format(traceback.format_exc()))
        message = "{} not found".format(key_path)
        logger.error(message)
        sys.exit()

    data = json.load(f)
    apiKey = data["apiKey"]
    apiSecret = data["apiSecret"]
    if (apiKey == "") or (apiSecret == ""):
        logger.error("API Keys Missing")
        sys.exit()

    try:
        Client.API_URL = endpoints[endpoint]
    except Exception as e:
        logger.error("Exception : '{}'".format(e))
        logger.error("Traceback : '{}'".format(traceback.format_exc()))
        message = "'{}' didn't work as endpoint name. If unsure let it be 'default' in config.json".format(
            endpoint
        )
        logger.error(message)
        sys.exit()

    logger.info("Creating Binance client...")
    # create binance Client
    client = Client(apiKey, apiSecret)

    # get balances for all assets & some account information
    logger.info(
        "Account information:\n{}".format(
            json.dumps(client.get_account(), indent=4, sort_keys=True)
        )
    )

    # get all symbols with coinPair as quote
    tickers = client.get_all_tickers()
    symbols = []
    for ticker in tickers:
        if quotedCoin in ticker["symbol"]:
            symbols.append(ticker["symbol"])

    # cache average prices
    logger.info(
        "Caching all {} pairs average prices...This can take a while.".format(
            quotedCoin
        )
    )
    tickers = client.get_ticker()
    averagePrices = []
    for ticker in tickers:
        if quotedCoin in ticker["symbol"]:
            averagePrices.append(
                dict(symbol=ticker["symbol"], wAvgPrice=ticker["weightedAvgPrice"])
            )

    # getting btc conversion
    response = requests.get("https://api.coindesk.com/v1/bpi/currentprice.json")
    data = response.json()
    in_USD = float((data["bpi"]["USD"]["rate_float"]))

    # find amount of bitcoin to use
    try:
        QuotedBalance = float(client.get_asset_balance(asset=quotedCoin)["free"])
    except Exception as e:
        logger.error("Exception : '{}'".format(e))
        logger.error("Traceback : '{}'".format(traceback.format_exc()))
        message = "Invalid API Keys maybe?"
        logger.error(message)
        sys.exit()

    # decide if use percentage or manual amount
    if manualQuoted <= 0:
        AmountToSell = round_decimals_up(QuotedBalance * percentOfWallet)
    else:
        AmountToSell = round_decimals_up(manualQuoted)

    if AmountToSell * limitLoss < common_min_order_size:
        logger.warning(
            "The amount to buy ({} {}) seems too low. The common minimum for a transaction is {}. \nBUT since we plan on setting a lower volume stop loss, you should at least buy {} BTC. \nThe actual minimum is coin dependent, check 'binance.com/en/trade-rule'".format(
                AmountToSell,
                quotedCoin,
                common_min_order_size,
                float_to_string_up(common_min_order_size / limitLoss),
            )
        )

    # nice user message
    logger.info(
        """ 
 |  __ \                     (_)   | |            
 | |__) |   _ _ __ ___  _ __  _  __| | ___  _   _ 
 |  ___/ | | | '_ ` _ \| '_ \| |/ _` |/ _ \| | | |
 | |   | |_| | | | | | | |_) | | (_| | (_) | |_| |
 |_|    \__,_|_| |_| |_| .__/|_|\__,_|\___/ \__,_|
                       | |                        
                       |_|                        """
    )
    # wait until coin input
    logger.info(
        "Investing amount for {}: {} ({}% of your wallet containing {} {})".format(
            quotedCoin, float_to_string_up(AmountToSell), percentOfWallet, QuotedBalance, quotedCoin
        )
    )
    logger.info(
        "Investing amount in USD: {}".format(
            float_to_string_up((in_USD * AmountToSell), 2)
        )
    )

    tradingPair = input("Coin pair: ").upper() + quotedCoin

    # get trading pair price
    try:
        price = float(client.get_avg_price(symbol=tradingPair)["price"])
    except BinanceAPIException as e:
        if e.code == -1121:
            logger.error(
                "Invalid trading pair given. Check your input is correct as well as config.json's 'coinPair' value to "
                "fix the error."
            )
            logger.error("Invalid trading pair given.")
        else:
            logger.error("A BinanceAPI error has occurred. Code = " + str(e.code))
            logger.error(
                e.message
                + ". Please use https://github.com/binance/binance-spot-api-docs/blob/master/errors.md to find "
                "greater details on error codes before raising an issue."
            )
            logger.error(
                "Binannce API error occured on getting price for trading pair."
            )
        sys.exit()
    except Exception as e:
        logger.error("Exception : '{}'".format(e))
        logger.error("Traceback : '{}'".format(traceback.format_exc()))
        message = "Unknown error has occured while getting price for trading pair"
        logger.error(message)
        sys.exit()

    logger.info("price={}".format(price))
    # calculate amount of coin to buy
    amountOfCoin = AmountToSell / price

    # ensure buy limit is setup correctly
    averagePrice = 0
    for ticker in averagePrices:
        if ticker["symbol"] == tradingPair:
            averagePrice = ticker["wAvgPrice"]
    if averagePrice == 0:
        averagePrice = price

    # rounding the coin amount to the specified lot size
    info = client.get_symbol_info(tradingPair)
    minQty = float(info["filters"][2]["minQty"])
    maxQty = float(info["filters"][2]["maxQty"])
    stepSize = float(info["filters"][2]["stepSize"])
    amountOfCoin = float_to_string_down(amountOfCoin, int(-math.log10(stepSize)))

    # rounding price to correct dp
    minPrice = float(info["filters"][0]["minPrice"])
    tickSize = float(info["filters"][0]["tickSize"])
    minNotional = float(info["filters"][3]["minNotional"])

    if AmountToSell < minNotional:
        logger.error(
            "The amount to buy ({} {}) IS too low. The minimum is {}. We'll keep going but this should fail terribly.".format(
                AmountToSell, quotedCoin, minNotional
            )
        )
    if float(amountOfCoin) > maxQty:
        logger.error(
            "You're trying to buy {} coins when the maximum allowed is {}".format(
                amountOfCoin, maxQty
            )
        )

    logger.info("average price before pump '{}'".format(averagePrice))

    maxBuyPrice = float(averagePrice) * buyLimit
    maxBuyPrice = float_to_string(maxBuyPrice, int(-math.log10(tickSize)))
    logger.info("Max buy price '{}'".format(maxBuyPrice))
    logger.info("Will ask for {} {}".format(amountOfCoin, tradingPair))
    # TODO check minNotional and min value

    # logger.info("get_symbol_info: \n{}".format(json.dumps(info, indent=4, sort_keys=True)))
    logger.info("filters {}".format(info["filters"]))

    try:
        # buy order
        order = client.order_limit_buy(
            symbol=tradingPair, quantity=amountOfCoin, price=maxBuyPrice
        )
        logger.info(
            "Buy order has been made. Asked for {} {} at max price {} (avg price: {})".format(
                amountOfCoin, tradingPair, maxBuyPrice, averagePrice
            )
        )
    except BinanceAPIException as e:
        logger.error("A BinanceAPI error has occurred. Code = " + str(e.code))
        logger.error(
            e.message
            + ". Please use https://github.com/binance/binance-spot-api-docs/blob/master/errors.md to find "
            "greater details on error codes before raising an issue."
        )
        logger.error("Binance API error has occured on buy order")
        sys.exit()
    except Exception as e:
        logger.error("Exception : '{}'".format(e))
        logger.error("Traceback : '{}'".format(traceback.format_exc()))
        message = "Unknown error has occured while sending buy order"
        logger.error(message)
        sys.exit()

    # waits until the buy order has been confirmed
    try:
        orderId = order['orderId']
        logger.info('Buy order placed with id = #%d' % orderId)
        while order["status"] != "FILLED":
            try:
                order = client.get_order(symbol=tradingPair, orderId=orderId)
                logger.info("Waiting for coin to buy (current status: %s, price: %s)..." % (order['status'], order['price']))
            except BinanceAPIException:
                logger.info('API error while trying to retrieve order, retrying...')
    except KeyboardInterrupt:
        logger.warning('CTRL+C, removing buy order and exiting')
        client.cancel_order(symbol=tradingPair, orderId=orderId)
        order = client.get_order(symbol=tradingPair, orderId=orderId)
        logger.info(order)
        exit()

    # once bought we can get info of order
    if 'fills' in order:
        coinOrderInfo = order["fills"][0]
        coinPriceBought = float(coinOrderInfo["price"])
        coinOrderQty = float(coinOrderInfo["qty"])
    else:
        coinPriceBought = float(order["cummulativeQuoteQty"]) / float(order["executedQty"])
        coinOrderQty = float(order["executedQty"])

    logger.info(
        "We bought {} {} at a price of {}".format(
            coinOrderQty, tradingPair, coinPriceBought
        )
    )

    # once finished waiting for buy order we can process the sell order
    logger.info("Processing sell order.")
    # rounding sell price to correct dp
    priceToSell = coinPriceBought * profitMargin
    roundedPriceToSell = float_to_string(priceToSell, int(-math.log10(tickSize)))

    # TODO stopPrice!=stopLimitPrice
    try:
        # oco order (with stop loss)
        stopPrice = float_to_string(
            stopLoss * coinPriceBought, int(-math.log10(minPrice))
        )
        stopLimitPrice = float_to_string(
            limitLoss * coinPriceBought, int(-math.log10(minPrice))
        )
        order = client.create_oco_order(
            symbol=tradingPair,
            quantity=coinOrderQty,
            side=SIDE_SELL,
            price=roundedPriceToSell,
            stopPrice=stopPrice,
            stopLimitPrice=stopLimitPrice,
            stopLimitTimeInForce=TIME_IN_FORCE_GTC,
        )
    except BinanceAPIException as e:
        logger.error("A BinanceAPI error has occurred. Code = " + str(e.code))
        logger.error(
            e.message
            + ". Please use https://github.com/binance/binance-spot-api-docs/blob/master/errors.md to find "
            "greater details "
            "on error codes before raising an issue."
        )
        logger.error("Binance API error has occured on sell order")
        sys.exit()
    except Exception as e:
        logger.error("Exception : '{}'".format(e))
        logger.error("Traceback : '{}'".format(traceback.format_exc()))
        message = "Unknown error has occured while doing the OCO sell order"
        logger.error(message)
        sys.exit()

    logger.info(
        "Sell order has been made! Trying to sell {} {} at min price {}".format(
            coinOrderQty, tradingPair, stopLimitPrice
        )
    )

    logger.info(
        "Account information:\n{}".format(
            json.dumps(client.get_account(), indent=4, sort_keys=True)
        )
    )

    logger.info("All good! Opening webpage and closing the program.")
    # open binance page to trading pair
    webbrowser.open("https://www.binance.com/en/trade/" + tradingPair)

    sys.exit()


if __name__ == "__main__":
    pump_and_dump()
